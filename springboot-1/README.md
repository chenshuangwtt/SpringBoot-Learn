#SpringBoot初始教程之项目结构
##1 简介
Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run". We take an opinionated view of the Spring platform and third-party libraries so you can get started with minimum fuss. Most Spring Boot applications need very little Spring configuration.

Features

1. Create stand-alone Spring applications
2. Embed Tomcat, Jetty or Undertow directly (no need to deploy WAR files)
3. Provide opinionated 'starter' POMs to simplify your Maven configuration
4. Automatically configure Spring whenever possible
5. Provide production-ready features such as metrics, health checks and externalized configuration
6. Absolutely no code generation and no requirement for XML configuration

上述是官方的英文讲解、大概翻译后如下：

SpringBoot在建立生产中的独立程序上非常简便、只需要一些简便的配置就能运行起来。大致有如下特点：

1. 创建独立的Spring applications
2. 能够使用内嵌的Tomcat, Jetty or Undertow，不需要部署war
3. 提供starter pom来简化maven配置
4. 自动配置Spring
5. 提供一些生产环境的特性，比如metrics, health checks and externalized configuration
6. 绝对没有代码生成和XML配置要求

##2. 快速开始

```xml

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.4.1.RELEASE</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>springboot-1</artifactId>


    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>1.4.1.RELEASE</version>
            </plugin>
        </plugins>
    </build>
</project>

```

这块统一用spring-boot-starter-parent做为parent 所以`spring-boot-starter-web`不用填写版本号

```xml

<parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.4.1.RELEASE</version>
 </parent>
    
```

下面这个插件是用来运行Springboot的，通常有两种方式可以运行SpringBoot，一种是通过直接运行main方法，另外一种是通过使用下面的插件运行。
两种方式有差别，一旦项目中需要访问资源的时候就需要通过插件运行，否则无法访问到资源
```xml

<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <version>1.4.1.RELEASE</version>
</plugin>

```

上面是maven配置，接下来需要配置整个程序的入口，`AppApplication`

```java

@SpringBootApplication
public class AppApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(AppApplication.class, args);
    }
}

```

下面写了一个简易的controller，只要运行main方法或者插件，就能够正常访问了。这块需要注意，controller跟AppApplication放在一个包下面，
如果不再一个下面需要配置扫描的包

```java

@RestController
public class IndexController {
    @GetMapping("/index")
    public ResponseEntity helloWord() {
        return ResponseEntity.ok("hello word");
    }
}

```



###3.配置详解

`@SpringBootApplication` 这个注解是一个组合注解，聚合了多个注解的功能,具体源码如下

```java

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class))
public @interface SpringBootApplication {

	//排除自启动项
	Class<?>[] exclude() default {};

	//排除自动启动的beanName
	String[] excludeName() default {};

	   //扫描包
	@AliasFor(annotation = ComponentScan.class, attribute = "basePackages")
	String[] scanBasePackages() default {};
    
	//扫描类
	@AliasFor(annotation = ComponentScan.class, attribute = "basePackageClasses")
	Class<?>[] scanBasePackageClasses() default {};

}

```
`@EnableAutoConfiguration`这个注解是用来启动SpringBoot中的自动配置项目，这个注解是必须加上的，否则无法正常使用因为SpringBoot
默认配置了很多配置项,如下图所示配置在这里面的配置项都会自动进行配置，一部分需要引用其他jar包配置才会生效

![Renderings](https://git.oschina.net/wangkang_daydayup/SpringBoot-Learn/raw/master/springboot-1/images/QQ20161107-0@2x.png?dir=0&filepath=springboot-1%2Fimages%2FQQ20161107-0%402x.png&oid=0f8ba841a88cd1f5b5e16efff1f44ddb38e02388&sha=99cea800a7e1849b1d8365f0fdb8ab8114c852fe)

###application.yaml 文件详解
application.yaml是整个应用程序的配置文件，SpringBoot自动加载，SpringBoot提供针对各种组件的都可以通过application.yaml进行配置，后续再进行详解。
![Renderings](https://git.oschina.net/wangkang_daydayup/SpringBoot-Learn/raw/master/springboot-1/images/QQ20161107-2@2x.png?dir=0&filepath=springboot-1%2Fimages%2FQQ20161107-2%402x.png&oid=fcca1f5c72e1d0d0220f8ce16bcd896c1619f71c&sha=2890ddb83dbff79b257cf59e3e2793095cc47825)
