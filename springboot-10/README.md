#SpringBoot初始教程之SpringBoot-Metrics监控(十)
##1.介绍
Metrics基本上是成熟公司里面必须做的一件事情，简单点来说就是对应用的监控，之前在一些技术不成熟的公司其实是不了解这种概念，因为业务跟技术是相关的
当业务庞大起来，技术也会相对复杂起来，对这些复杂的系统进行监控就存在必要性了，特别是在soa化的系统中，完整一个软件的功能分布在各个系统中，针对这些功能进行监控就更必要了
而Spring Boot Actuator 提供了metrics service，让监控变得统一化了，方便管理
##2.快速开始
核心是`spring-boot-starter-actuator` 这个依赖，增加这个依赖之后SpringBoot就会默认配置一些通用的监控，比如jvm监控、类加载、http监控，
当然这些都是一些简单的监控，理论上来说大部分若要在生产中使用，还是需要定制化一下的。先来看一下pom
####pom.xml
```xml

    <?xml version="1.0" encoding="UTF-8"?>
    <project xmlns="http://maven.apache.org/POM/4.0.0"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <parent>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>1.4.1.RELEASE</version>
        </parent>
        <modelVersion>4.0.0</modelVersion>
    
        <artifactId>springboot-10</artifactId>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-test</artifactId>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-jetty</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-actuator</artifactId>
            </dependency>
        </dependencies>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>1.4.1.RELEASE</version>
                    <configuration>
                        <fork>true</fork>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
            </plugins>
        </build>
    
    </project>

```

####启动类
```java

    package com.start;
    
    import org.springframework.boot.SpringApplication;
    import org.springframework.boot.autoconfigure.SpringBootApplication;
    
    /**
     * ClassName: AppApplication
     * Description:
     *
     * @author kang.wang03
     *         Date 2016/11/8
     */
    @SpringBootApplication
    public class AppApplication {
    
        public static void main(String[] args) throws Exception {
            SpringApplication.run(AppApplication.class, args);
        }
    
    }

```

上述配置完成以后运行程序，你会发觉打印日志的时候多了很多mapping的日志

```

    org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
    2016-11-21 15:48:53.733  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/heapdump || /heapdump.json],methods=[GET],produces=[application/octet-stream]}" onto public void org.springframework.boot.actuate.endpoint.mvc.HeapdumpMvcEndpoint.invoke(boolean,javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse) throws java.io.IOException,javax.servlet.ServletException
    2016-11-21 15:48:53.734  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/health || /health.json],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.HealthMvcEndpoint.invoke(java.security.Principal)
    2016-11-21 15:48:53.739  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/info || /info.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.740  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/beans || /beans.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.742  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/env/{name:.*}],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EnvironmentMvcEndpoint.value(java.lang.String)
    2016-11-21 15:48:53.742  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/env || /env.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.743  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/dump || /dump.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.745  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/mappings || /mappings.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.746  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/trace || /trace.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.749  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/autoconfig || /autoconfig.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.752  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/configprops || /configprops.json],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.EndpointMvcAdapter.invoke()
    2016-11-21 15:48:53.754  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/metrics/{name:.*}],methods=[GET],produces=[application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.mvc.MetricsMvcEndpoint.value(java.lang.String)
    2016-11-21 15:48:53.754  INFO 3883 --- [           main] o.s.b.a.e.mvc.EndpointHandlerMapping     : Mapped "{[/metrics || /metrics.json],methods=[GET],

```
###2.1 /heapdump

`/heapdump`这个主要会dump目前堆的状况出来，可以用jvm的工具大家，并查看目前堆的状况，但是总的来说没人会这么干，会造成系统的短暂停止，大流量容易压垮系统

###2.2 /health
`/health` 这个路径主要是用来统计系统的状况，默认里面目前只有系统状况和磁盘状况
```json

    {
        "status": "UP",
        "diskSpace": {
            "status": "UP",
            "total": 120108089344,
            "free": 20677521408,
            "threshold": 10485760
        }
    }
    
```
###2.3 /info

`/info`

###2.4 /beans

`/beans`可以查看到目前Spring里面加载的所有bean，在生产中感觉没有什么用处，可能在开发中会有一些帮助，方便查看bean是否被扫描

###2.5 /env

`/env`里面包含了目前的环境变量，包括`application.yaml`配置的属性，以及systemProperties都能够拿到

###2.6 /dump

`/dump`里面包含了目前线程的一个快照信息

###2.7 /mappings

`/mappings` 里面包含了Controller的所有mapping信息，开发中新手经常会遇到访问不到controller的情况，可以根据这个查看是否被扫描

###2.8 /trace

`/trace`  trace目前主要是监控http请求的,监控每个请求的状况，如下所示：
```
[
    {
        "timestamp": 1479717912091,
        "info": {
            "method": "GET",
            "path": "/",
            "headers": {
                "request": {
                    "Connection": "keep-alive",
                    "User-Agent": "GoogleSoftwareUpdateAgent/1.2.6.1370 CFNetwork/807.0.4 Darwin/16.0.0 (x86_64)",
                    "Host": "127.0.0.1:8080"
                },
                "response": {
                    "X-Application-Context": "application",
                    "Date": "Mon, 21 Nov 2016 08:45:12 GMT",
                    "Content-Type": "application/json;charset=UTF-8",
                    "status": "404"
                }
            }
        }
    },
    {
        "timestamp": 1479717910980,
        "info": {
            "method": "GET",
            "path": "/",
            "headers": {
                "request": {
                    "Connection": "keep-alive",
                    "User-Agent": "GoogleSoftwareUpdate/1.2.6.1370 CFNetwork/807.0.4 Darwin/16.0.0 (x86_64)",
                    "Host": "127.0.0.1:8080"
                },
                "response": {
                    "X-Application-Context": "application",
                    "Date": "Mon, 21 Nov 2016 08:45:10 GMT",
                    "Content-Type": "application/json;charset=UTF-8",
                    "status": "404"
                }
            }
        }
    },
    {
        "timestamp": 1479717908924,
        "info": {
            "method": "GET",
            "path": "/",
            "headers": {
                "request": {
                    "Connection": "keep-alive",
                    "User-Agent": "GoogleSoftwareUpdate/1.2.6.1370 CFNetwork/807.0.4 Darwin/16.0.0 (x86_64)",
                    "Host": "127.0.0.1:8080"
                },
                "response": {
                    "X-Application-Context": "application",
                    "Date": "Mon, 21 Nov 2016 08:45:08 GMT",
                    "Content-Type": "application/json;charset=UTF-8",
                    "status": "404"
                }
            }
        }
    },
    {
        "timestamp": 1479716651440,
        "info": {
            "method": "GET",
            "path": "/trace",
            "headers": {
                "request": {
                    "Cache-Control": "max-age=0",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                    "Upgrade-Insecure-Requests": "1",
                    "Connection": "keep-alive",
                    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36",
                    "Host": "localhost:8080",
                    "Accept-Encoding": "gzip, deflate, sdch, br",
                    "Accept-Language": "zh-CN,zh;q=0.8,en;q=0.6"
                },
                "response": {
                    "X-Application-Context": "application",
                    "Date": "Mon, 21 Nov 2016 08:24:11 GMT",
                    "Content-Type": "application/json;charset=UTF-8",
                    "status": "200"
                }
            }
        }
    }
]
```

###2.9 /autoconfig
`/autoconfig` 显示当前SpringBoot，已经自动配置的的属性

###2.10 /metrics
/metrics 是整个监控里面的核心信息，
```json

    {
        "mem": 323984,
        "mem.free": 239989,
        "processors": 4,
        "instance.uptime": 1670490,
        "uptime": 1691378,
        "systemload.average": 4.22265625,
        "heap.committed": 271872,
        "heap.init": 131072,
        "heap.used": 31882,
        "heap": 1864192,
        "nonheap.committed": 54056,
        "nonheap.init": 2496,
        "nonheap.used": 52113,
        "nonheap": 0,
        "threads.peak": 15,
        "threads.daemon": 5,
        "threads.totalStarted": 17,
        "threads": 15,
        "classes": 6662,
        "classes.loaded": 6662,
        "classes.unloaded": 0,
        "gc.ps_scavenge.count": 10,
        "gc.ps_scavenge.time": 153,
        "gc.ps_marksweep.count": 2,
        "gc.ps_marksweep.time": 150,
        "gauge.response.trace": 19,
        "gauge.response.autoconfig": 41,
        "gauge.response.error": 6,
        "gauge.response.configprops": 138,
        "counter.status.200.configprops": 1,
        "counter.status.404.error": 3,
        "counter.status.200.autoconfig": 1,
        "counter.status.200.trace": 2
    }

```
目前来说包含了如下信息

```
    The total system memory in KB (mem)
    The amount of free memory in KB (mem.free)
    The number of processors (processors)
    The system uptime in milliseconds (uptime)
    The application context uptime in milliseconds (instance.uptime)
    The average system load (systemload.average)
    Heap information in KB (heap, heap.committed, heap.init, heap.used)
    Thread information (threads, thread.peak, thread.daemon)
    Class load information (classes, classes.loaded, classes.unloaded)
    Garbage collection information (gc.xxx.count, gc.xxx.time)

```

##3 总结

这些是SpringBoot提供的一些比较简单的Metrics，其实在生产中可以借鉴一些，但是大多数基本都用不上，SpringBoot针对DataSource和Cache都做了Metrics，这个在之后的中级篇会做更进一步的讲解

