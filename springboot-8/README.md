#SpringBoot初始教程之测试(八)
##1.介绍
在为参加工作之前也写过测试用例，当时主要是针对外包项目的，其实总的来说主要是用来跑数据当做main方法使用，但基本不经常写，随着工作年限的增长。
渐渐的写了很多测试用例，主要是针对以下两种原因：

 1. 项目代码量庞大，每次迭代的任务不是很多。
 2. 公司有发布系统，每次提交代码到git都会触发编译，部署。
 3. 为了保障改了之后的代码能够正常运行，至少不报明显错误。
 4. 基本soa框架下面的开发并不太好启动项目调试，因为没有入口，也没有controller，只有一堆service 
 
针对以上的问题所以就慢慢的开始写了测试，当然只是写一些简单的测试用例，具体的测试还是需要专业测试来进行测试。

OK回到主题，Spring Boot提供了很多方便使用的测试方式，针对测试增加了一个依赖`spring-boot-starter-test` 方便写测试用例

##2.快速开始
###2.1 Service接口测试
SpringBoot 提供一个注解`@SpringBootTest`，它能够测试你的SpringApplication，因为SpringBoot程序的入口是SpringApplication，
基本的所有配置都会通过入口类去加载，而注解可以引用入口类的配置。

``java

    @RunWith(SpringJUnit4ClassRunner.class)
    //指定SpringBoot工程的Application启动类
    //支持web项目
    @WebAppConfiguration
    @SpringBootTest(classes = AppApplication.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
    public class IndexControllerTest {
        @Autowired
        private IndexService indexService;
    
        @Test
        public void indexServiceTest() {
            String body = indexService.getStr();
            assertThat(body).isEqualTo("ok");
        }
    }


```
这个是一个简单的Service测试，也是最开发中最常用的测试方式，直接注入Service调用方法能够获取到放回的内容，并且校验内容是否正确，`@SpringBootTest`提供了如下webEnvironment配置

```
MOCK —提供一个Mock的Servlet环境，内置的Servlet容器并没有真实的启动，主要搭配使用@AutoConfigureMockMvc 
 
RANDOM_PORT — 提供一个真实的Servlet环境，也就是说会启动内置容器，然后使用的是随机端口
DEFINED_PORT — 这个配置也是提供一个真实的Servlet环境，使用的默认的端口，如果没有配置就是8080
NONE — 这是个神奇的配置，跟Mock一样也不提供真实的Servlet环境。
        
```

###2.2 Rest 接口测试
这个是针对controller Rest接口的测试，如果返回的是一些json请求，或则其他内容，当然这种方式不太和谐
```java
    
    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = AppApplication.class,
            webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
    public class IndexControllerTest {
        @Autowired
        private TestRestTemplate restTemplate;
        //注入端口号
        @LocalServerPort
        private int port;
        @Test
        public void indexControllerTest() {
            String body = this.restTemplate.getForObject("/index", String.class);
            assertThat(body).isEqualTo("ok"+port);
        }
    }

```

###2.3 日志捕获测试
这个测试方式是最近看文章才发现的一种方式，也就说能够捕获在运行的程序中打印的日志。
当然这个有些场景是可以使用的，当一个测试方法里面调用了很多方法，可以通过打印这些方法的返回结果来得到测试结果

```java

    @RunWith(SpringJUnit4ClassRunner.class)
    //指定SpringBoot工程的Application启动类
    //支持web项目
    @WebAppConfiguration
    @SpringBootTest(classes = AppApplication.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
    public class OutputCaptureTest {
        private Log log = LogFactory.getLog(OutputCaptureTest.class);
        @Rule
        // 这里注意，使用@Rule注解必须要用public
        public OutputCapture capture = new OutputCapture();
    
        @Test
        public void testLogger() {
            System.out.println("sd");
            log.info("test");
            assertThat(capture.toString(), Matchers.containsString("test"));
        }
    
        @Test
        public void testSdLogger() {
            System.out.println("sd");
            assertThat(capture.toString(), Matchers.containsString("sd"));
        }
    
        @Test
        public void testNoSdLogger() {
            System.out.println("sd");
            assertThat(capture.toString(), Matchers.containsString("nod"));
        }
    }

```

###2.4 不同环境的测试
`@ActiveProfiles(profiles = "test") ` 在测试类上面指定profiles，可以改变当前spring 的profile，来达到多环境的测试 

###本文代码

https://git.oschina.net/wangkang_daydayup/SpringBoot-Learn/tree/master

springboot-8