#ELK安装指南
##快速开始
首先需要安装elasticsearch，elasticsearch是基于java进行开发的，所以需要机器上需要有JDK的环境，建议1.7以上
软件版本：

    1. elasticsearch-5.0.0
    2. logstash-2.4.0
    3. kibana-5.0.0

```shell
    
    
    
```

##1.elasticsearch 安装配置
1. 首先来处理elasticsearch的配置,先按照上图的方式把文件下载到/data/soft中
```shell
    mkdir -p /data/soft
    cd /data/soft && wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.0.0.zip
    ##解压
    unzip elasticsearch-5.0.0.zip
    cd /data/soft/elasticsearch-5.0.0
    vim  config/elasticsearch.yml
    ##修改network.host参数，变成对应的机器的ip地址,如果是本地测试 可以不用修改
    network.host: 10.165.111.12

```

配置好之后可以尝试运行一下,执行命令 `./bin/elasticsearch`,如果发现没有启动成功，并且提示以下信息。可以通过下面的方式解决

```shell

    max file descriptors [65535] for elasticsearch process likely too low, increase to at least [65536]
    max virtual memory areas vm.max_map_count [65530] likely too low, increase to at least [262144]

```

###1.1解决方案
```shell

 sysctl -w vm.max_map_count=262144
 echo "vm.max_map_count=262144" >> /etc/sysctl.conf
 
 #修改文件限制 
 vim /etc/security/limits.conf
 * soft nofile 65535
 * hard nofile 65535
 
 ###把上述两个参数的值修改成70000
 
```

2.安装elasticsearch head插件方便查看索引数据，该插件暂时不支持5.0的新方式，所以只能通过源码方式安装

```shell
    
    git clone git://github.com/mobz/elasticsearch-head.git
    cd elasticsearch-head
    #如果是ubuntu 则采用 apt-get install -y npm
    yum install -y npm
    npm install  --registry=http://registry.npm.taobao.org
    npm install  -g grunt-cli --registry=http://registry.npm.taobao.org
    grunt server

```

###1.2 运行elasticsearch
```shell

    sh /data/soft/elasticsearch-5.0.0/bin/elasticsearch -d
    
```
##2 logstash 安装配置

```shell

    cd /data/soft && wget https://download.elastic.co/logstash/logstash/logstash-2.4.0.tar.gz
    tar -zxvf logstash-2.4.0.tar.gz
    cd /data/soft/logstash-2.4.0/
    mkdir conf
    vim conf/logstash.conf
    
```            
加入配置nginx 配置

```shell

    input {
        file {
            path => ["/var/log/nginx/access.log"]
             }
    }
    filter {
        if [path] =~ "access" {
            grok {
                match => {"message" => "%{IPORHOST:source_ip} - %{USERNAME:remote_user} \[%{HTTPDATE:timestamp}\] %{QS:request} %{NUMBER:status} %{NUMBER:body_bytes_sent} %{QS:http_referer} %{QS:http_user_agent} %{QS:http_x_forwarded_for} %{NUMBER:request_time}"
                        }
                }
            mutate {
                convert => { "status" => "integer" }
            }
            mutate {
                convert => { "request_time" => "float" }
            }
            mutate {
        convert => { "body_bytes_sent" => "integer" }
            }
        }
    }
    output {
        elasticsearch {
            hosts => ["localhost:9200"]
            codec => "json"
            index => "logstash-lol6.nginx-%{+YYYY.MM.dd}"
            }
    }
```


pattern是针对nginx的日志模式进行过滤的，nginx这边增加了一个$request_time 参数用来统计nginx的相应时间
```shell
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" $request_time';
```

###2.1 启动logstash 

```shell
 nohup sh bin/logstash -f conf/logstash.conf &
```
##3. kibana 安装

```shell
    
    
    cd /data/soft && wget https://artifacts.elastic.co/downloads/kibana/kibana-5.0.0-linux-x86_64.tar.gz
    tar -zxvf kibana-5.0.0-linux-x86_64.tar.gz
    cd kibana-5.0.0-linux-x86_64/
    
```
由于kibana和elasticsearch 安装在一台机器上面，所以不用对kibana进行配置，kibana的配置在`kibana-5.0.0-linux-x86_64/config/kibana.yml`
如果有需求更改下elasticsearch的地址即可
###3.1 启动kibana

```shell
    cd /data/soft/kibana-5.0.0-linux-x86_64/bin/ && nohup sh kibana &
```

##4.扩展

过滤表达式校验

https://grokdebug.herokuapp.com/
