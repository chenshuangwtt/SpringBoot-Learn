package com.start;

import com.custom.MyBasicErrorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.Servlet;
import java.util.List;

/**
 * ClassName: ConfigSpringboot
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/10/14
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({Servlet.class, DispatcherServlet.class})
@AutoConfigureBefore(WebMvcAutoConfiguration.class)
@EnableConfigurationProperties(ResourceProperties.class)
public class ConfigSpringboot {
    @Autowired(required = false)
    private List<ErrorViewResolver> errorViewResolvers;
    private final ServerProperties serverProperties;

    public ConfigSpringboot(
            ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    @Bean
    public MyBasicErrorController basicErrorController(ErrorAttributes errorAttributes) {
        return new MyBasicErrorController(errorAttributes, this.serverProperties.getError(),
                this.errorViewResolvers);
    }


}
